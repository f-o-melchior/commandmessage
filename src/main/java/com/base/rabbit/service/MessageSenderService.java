package com.base.rabbit.service;

import com.base.rabbit.model.MessageEntity;

public interface MessageSenderService {

    void sendMessage(MessageEntity message, String status);

}
