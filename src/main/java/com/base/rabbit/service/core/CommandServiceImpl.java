package com.base.rabbit.service.core;


import com.base.rabbit.model.MessageEntity;
import com.base.rabbit.service.CommandService;
import com.base.rabbit.service.MessageSenderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CommandServiceImpl implements CommandService {

    Logger logger = LogManager.getLogger(CommandServiceImpl.class);

    @Autowired
    private MessageSenderService messagingService;

    @Value("${priority.message.status}")
    private String priority;

    @Value("${simple.message.status}")
    private String simple;

    @Override
    public void ececuteCommand(final String command) {
        if(command.startsWith("send message")){
            if(command.contains("-prio")){
                messagingService.sendMessage(new MessageEntity(getParts(command)[3], getParts(command)[4], getParts(command)[5]), priority);
            }else{
                messagingService.sendMessage(new MessageEntity(getParts(command)[2], getParts(command)[3], getParts(command)[4]), simple);
            }
        }else{
            logger.error("Unrecognized command: " + command);
        }
    }

    private String[] getParts(String command){
        return command.split(" ");
    }
}
