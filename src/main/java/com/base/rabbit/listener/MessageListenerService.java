package com.base.rabbit.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;


@Service
public class MessageListenerService {

    Logger logger = LogManager.getLogger(MessageListenerService.class);

    @RabbitListener(queues = "${rabbitmq.queue.name}")
    public void receiveMessage(final Message message) {
        logger.info("");
        logger.info("**** START - MESSAGE RECEIVED ****");
        logger.info(message.toString());
        logger.info("**** END - MESSAGE RECEIVED ****");
    }
}
