package com.base.rabbit.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class MessageEntity implements Serializable {

    private final String from;
    private final String to;
    private final String message;

    public MessageEntity(@JsonProperty("from")  String from,
                         @JsonProperty("to") String to,
                         @JsonProperty("message") String message) {
        this.from = Objects.requireNonNull(from, "FROM value can not be null");
        this.to = Objects.requireNonNull(to, "TO value can not be null");;
        this.message = Objects.requireNonNull(message, "MESSAGE value can not be null");;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

}
