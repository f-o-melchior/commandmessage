package com.base.rabbit.server;

import com.base.rabbit.service.CommandService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class CommandArgServer implements CommandLineRunner {

    Logger logger = LogManager.getLogger(CommandArgServer.class);

    @Autowired
    private CommandService commandService;

    @Override
    public void run(String... args) throws Exception {
        logger.info("server runs...");
        printInformation();

        Scanner scanner = new Scanner(System.in);
        getCommandPrompts(scanner);
    }


    private void getCommandPrompts(Scanner scanner) {
        String command = scanner.nextLine();
        commandService.ececuteCommand(command);
        getCommandPrompts(scanner);
    }

    private void printInformation(){

        StringBuilder info = new StringBuilder();
        info.append("\n \n");
        info.append("WELCOME TO RABBIT_MQ DEMO! \n");
        info.append("\n");
        info.append("Use these commands below: \n");
        info.append("    - to send a message: send message {from} {to} {message} \n");
        info.append("    - to set priority use '-prio' tag \n");
        info.append("    - example: send message -prio {from} {to} {message} \n");
        info.append("\n \n");

        System.out.println(info.toString());
    }

}
